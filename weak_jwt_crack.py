#!/usr/bin/python3

import jwt
import time
start_time = time.time()
encoded = str(input("JWT token : "))
path = str(input("Wordlist's path : "))
alg = str(input("Algorithm : "))
with open(path) as fin:
	for pw in fin:
		pw = pw.replace("\n","")
		try:
			ok = jwt.decode(encoded, str(pw), algorithms=[alg])
			print("Secret : " + str(pw))
			print("Payload : " + str(ok))
			break
		except:
			a = 1
print("--- %s seconds ---" % (time.time() - start_time))